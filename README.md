### eC-Placer Camera Viewer
By using the opencv, the script draw crosiers to each frame for different setups.

### Usage
The first step is to configure the ID of camera in opencv:

```python
cap = cv2.VideoCapture(0)
```
Then run the script.

The position of each crosier is changed when you click the mouse buttons.
* Left Mouse Click: Blue crosier
* Right Mouse Click: Green crosier
* Middle Mouse Click: Red crosier

To terminate the script press 'q'.

### License
License under the AGPL v3.

## Credits, Inspiration, Alternatives
Hackerspace.gr
