#!/bin/python3
# """
#   crosier.py: Draw crosiers to eC-placer camera capture

#   Copyright (C) 2020, Agis Zisimatos

#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.

#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
# """

from vidgear.gears import VideoGear
import cv2
import time
import sys

class DrawCrosier:
    """
    Store points form mouse callback of right, left and middle buttons which
    are the head, pcb and part
    """

    def __init__(self):
        """
        Class constructor
        """
        self.point_head = (-1, -1)
        self.point_pcb = (-1, -1)
        self.point_part = (-1, -1)

    def select_point(self, event, x, y, flags, param):
        """
        Store the position of mouse events like right, left and middle buttons

        :param event:
        :type event:
        :param x
        :type x:
        :param y:
        :type y:
        :param flags
        :type flags:
        :param param:
        :type param:
        """
        if event == cv2.EVENT_LBUTTONDOWN:
            self.point_head = (x, y)
        elif event == cv2.EVENT_RBUTTONDOWN:
            self.point_pcb = (x, y)
        elif event == cv2.EVENT_MBUTTONDOWN:
            self.point_part = (x, y)

    def draw_crosier(self, window_name, color_rgb, point):
        """
        Draw crossier in window_name with center in point and color with
        color_rgb

        :param window_name:
        :type window_name:
        :param color_rgb
        :type color_rgb:
        :param point:
        :type point:
        """
        cv2.line(window_name, (point[0], point[1]+5),
                 (point[0], point[1]+25), color_rgb, 2)
        cv2.line(window_name, (point[0]+5, point[1]),
                 (point[0]+25, point[1]), color_rgb, 2)
        cv2.line(window_name, (point[0]-5, point[1]),
                 (point[0]-25, point[1]), color_rgb, 2)
        cv2.line(window_name, (point[0], point[1]-5),
                 (point[0], point[1]-25), color_rgb, 2)

    def draw_line_vertical(self, window_name, color_rgb, point):
        """
        Draw vertical line in window_name with center in point and color with
        color_rgb

        :param window_name:
        :type window_name:
        :param color_rgb
        :type color_rgb:
        :param point:
        :type point:
        """
        cv2.line(window_name, (point[0], point[1]-1000),
                 (point[0], point[1]+1000), color_rgb, 2)

    def draw_line_horizontal(self, window_name, color_rgb, point):
        """
        Draw horizontal line in window_name with center in point and color with
        color_rgb

        :param window_name:
        :type window_name:
        :param color_rgb
        :type color_rgb:
        :param point:
        :type point:
        """
        cv2.line(window_name, (point[0]-1000, point[1]),
                 (point[0]+1000, point[1]), color_rgb, 2)

def usage():
    print("usage: crosier.py <video index 1> <video index 2>")

if __name__ == '__main__':
    if len(sys.argv) < 3:
        usage()
        sys.exit(1)
    print("Using video: ", sys.argv[1], sys.argv[2])
    # define and start the stream on first source ( For e.g #0 index device)
    stream1 = VideoGear(source=int(sys.argv[1]), logging=True).start()

    # define and start the stream on second source ( For e.g #1 index device)
    stream2 = VideoGear(source=int(sys.argv[2]), logging=True).start()

    # Init frame window
    cv2.namedWindow('Output Frame1', flags=cv2.WINDOW_GUI_NORMAL)
    cv2.setWindowTitle('Output Frame1', 'eC-placer')
    cv2.namedWindow('Output Frame2', flags=cv2.WINDOW_GUI_NORMAL)
    cv2.setWindowTitle('Output Frame2', 'eC-placer')
    # Init class for coordinate storing
    draw_crosier_a = DrawCrosier()
    draw_crosier_b = DrawCrosier()
    cv2.setMouseCallback('Output Frame1', draw_crosier_a.select_point)
    cv2.setMouseCallback('Output Frame2', draw_crosier_b.select_point)

    # infinite loop
    while True:

        frameA = stream1.read()
        # read frames from stream1

        frameB = stream2.read()
        # read frames from stream2

        # check if any of two frame is None
        if frameA is None or frameB is None:
            #if True break the infinite loop
            break

        # Draw lines
        draw_crosier_a.draw_line_vertical(frameA, (255, 0, 0), draw_crosier_a.point_head)
        draw_crosier_a.draw_line_horizontal(frameA, (0, 255, 0), draw_crosier_a.point_pcb)
        # draw_crosier_a.draw_crosier(frameA, (0, 0, 255), draw_crosier_a.point_part)

        draw_crosier_b.draw_line_vertical(frameB, (255, 0, 0), draw_crosier_b.point_head)
        draw_crosier_b.draw_line_horizontal(frameB, (0, 255, 0), draw_crosier_b.point_pcb)
        # draw_crosier_b.draw_crosier(frameB, (0, 0, 255), draw_crosier_b.point_part)

        # do something with both frameA and frameB here
        cv2.imshow("Output Frame1", frameA)
        cv2.imshow("Output Frame2", frameB)
        # Show output window of stream1 and stream 2 seperately

        key = cv2.waitKey(1) & 0xFF
        # check for 'q' key-press
        if key == ord("q"):
            #if 'q' key-pressed break out
            break

        if key == ord("w"):
            #if 'w' key-pressed save both frameA and frameB at same time
            cv2.imwrite("Image-1.jpg", frameA)
            cv2.imwrite("Image-2.jpg", frameB)
            #break   #uncomment this line to break out after taking images

    cv2.destroyAllWindows()
    # close output window

    # safely close both video streams
    stream1.stop()
    stream2.stop()
